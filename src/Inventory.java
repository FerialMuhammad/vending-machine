
import org.omg.CORBA.Object;
import java.util.HashMap;

public class Inventory<T> {
    protected HashMap<T,Integer> inventory= new HashMap<>();
    Inventory(){

    }

    public final void setInitial(T obj,int num){
        inventory.put(obj,num);
    }

    public void addObject(T obj,int num){
        this.inventory.put(obj,num);
    }

    public void removeObjects(T obj,int num) {
        if (this.inventory.get(obj) >= num) {


            this.inventory.replace(obj, (this.inventory.get(obj) - num));
        }
        else {
            System.out.println("The inventory doesn't have that many items, current count is: "+this.inventory.get(obj));
        }
    }
    public boolean contains(T obj){
        if(this.inventory.containsKey(obj)){
            return true;
        }
        else{return false;}

    }
    public int getQuantity(T obj){
        return this.inventory.get(obj);
    }

    public void showInventory(T item){
        if(contains(item)){
            System.out.println(getQuantity(item));
        }
        else{
            System.out.println("This item is not available");
        }

    }
    public int getSize(){
        return this.inventory.size();
    }

}
