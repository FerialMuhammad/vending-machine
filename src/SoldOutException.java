public class SoldOutException extends RuntimeException {
    private String message;
    SoldOutException(String msg){
        this.message = msg;
    }
    @Override
    public String getMessage(){
        return this.message;
    }
}
