public enum Item {
    CANDY("Candy",10), SNACK("Snack",50), NUTS("Nuts",90), Coke("Coke",25), Pepsi("Pepsi",35), Soda("Soda",45);
    protected String item;
    private int cost;
    Item(String name,int cost){

            this.item = name;
            this.cost = cost;

    }
    public String getName(){
        return this.item;
    }

    public int getCost(){
        return this.cost;
    }
    }

