public class NoChangeAvailableException extends RuntimeException {
    private String message;
    NoChangeAvailableException(String msg){
        this.message = msg;
    }
    @Override
    public String getMessage(){
        return this.message;
    }
}
