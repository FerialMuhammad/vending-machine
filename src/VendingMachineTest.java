public class VendingMachineTest {
    public static void main(String[] arg){
        VendingMachine VenMac = new VendingMachine();
        VenMac.displayItems();
        VenMac.selectItem(Item.CANDY,2);
        VenMac.selectItem(Item.Coke,1);
        VenMac.selectItem(Item.SNACK,1);
        VenMac.removeItem(Item.SNACK);
        VenMac.getOwed();
        VenMac.insertCoin(Coin.oneDollar);
        VenMac.Pay();
        VenMac.Outlet();
    }
}
