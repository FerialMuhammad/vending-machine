import java.util.ArrayList;
import java.util.HashMap;

public class VendingMachine {
    protected static Inventory <Coin> moneyInventory = new Inventory<>();
    protected static Inventory <Item> itemInventory = new Inventory<>();
    private ArrayList<Item> items;
    private ArrayList<Coin> coins = new ArrayList<>();
    VendingMachine(){

        for (Item i : Item.values()){
            itemInventory.addObject(i,100);
        }
        for (Coin c : Coin.values()){
            moneyInventory.addObject(c,10);
        }
        this.items=new ArrayList<Item>();
    }
    public void displayItems(){
        for(Item i:Item.values()){
            System.out.print(i.getName() + "\t");
            System.out.println(i.getCost());
        }
    }

    public void newItem(Item item,int number){
        this.itemInventory.addObject(item,number);
    }


    public void selectItem(Item item,int num){
        if(VendingMachine.itemInventory.contains(item)) {
            if (VendingMachine.itemInventory.getQuantity(item) >= num) {
                for (int i=0 ; i<num ; i++){
                    this.items.add(item);}

            } else {
                throw new SoldOutException("Not enough items for your request available, available quantity is: " + VendingMachine.itemInventory.getQuantity(item));
            }
        }
        else{
            throw new SoldOutException("This item is sold out");
        }
    }
    public void removeItem(Item item){
        if (this.items.contains(item)){
        this.items.remove(item);}
}
    public int getOwed(){
        int total=0;
        for(int i=0; i<this.items.size();i++){
            total+=this.items.get(i).getCost();

        }
        return total;
    }
    public void insertCoin(Coin coin){

        coins.add(coin);
        moneyInventory.addObject(coin,1);
    }
    public ArrayList<Coin> Pay() {
        int total = 0;
        for (Coin c : coins) {
            total += c.getValue();
        }
        this.coins.clear();
        getBill(this.items , getOwed(),total);

        return isSufficient(total);
    }

    private ArrayList<Coin> isSufficient(int T){
        ArrayList<Coin> change = new ArrayList<>();
        if (getOwed() == T){
            for(int i=0; i<this.items.size();i++){
                VendingMachine.itemInventory.removeObjects(this.items.get(i),1);
            }
        }
        else if(getOwed()>T){
            throw new NotSufficientPaidException("You still owe: " + (getOwed()-T));
        }
        else {
            change = getChange(T - getOwed());
            for (int i = 0; i < this.items.size(); i++) {
                VendingMachine.itemInventory.removeObjects(this.items.get(i), 1);
            }
        }
        return change;
    }
    private void getBill(ArrayList<Item> i,int c,int p){
        Bill B = new Bill(i,c,p);
        B.showBill();
    }

    public ArrayList<Coin> getChange(int change){
       ArrayList<Coin> Change = new ArrayList<>();
       int ch = change;
       while (ch >=Coin.twoDollars.getValue() && moneyInventory.contains(Coin.twoDollars) ){
               ch -= Coin.twoDollars.getValue();
               Change.add(Coin.twoDollars);
               moneyInventory.removeObjects(Coin.twoDollars,1);
           }
       while (ch >=Coin.oneDollar.getValue() && moneyInventory.contains(Coin.oneDollar)){
           ch -= Coin.twoDollars.getValue();
           Change.add(Coin.oneDollar);
           moneyInventory.removeObjects(Coin.oneDollar,1);
       }
        while (ch >=Coin.Quarter.getValue() && moneyInventory.contains(Coin.Quarter)){
            ch -= Coin.Quarter.getValue();
            Change.add(Coin.Quarter);
            moneyInventory.removeObjects(Coin.Quarter,1);
        }
        while (ch >=Coin.Dime.getValue() && moneyInventory.contains(Coin.Dime)){
            ch -= Coin.Dime.getValue();
            Change.add(Coin.Dime);
            moneyInventory.removeObjects(Coin.Dime,1);
        }
        while (ch >=Coin.Nickel.getValue() && moneyInventory.contains(Coin.Nickel)){
            ch -= Coin.Nickel.getValue();
            Change.add(Coin.Nickel);
            moneyInventory.removeObjects(Coin.Nickel,1);
        }
        while (ch >=Coin.Penny.getValue() && moneyInventory.contains(Coin.Penny)){
            ch -= Coin.Penny.getValue();
            moneyInventory.removeObjects(Coin.Penny,1);
        }
        if (ch==0){
            return Change;
        }
        else{
            throw new NoChangeAvailableException("No enough change available");
        }

    }

    public ArrayList<Item> Outlet(){
        ArrayList<Item> outlet;
        outlet = this.items;
        this.items.clear();
        return outlet;
    }
    public ArrayList<Coin> refund(){
        ArrayList<Coin> refund;
        refund = this.coins;
        this.coins.clear();
        return refund;
    }
}
