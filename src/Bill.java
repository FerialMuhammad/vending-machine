import java.util.ArrayList;

public class Bill {
    private ArrayList<Item> items;
    private int cost;
    private int Paid;
    Bill(ArrayList<Item> item, int Cost, int Paid){
        this.items = item;
        this.cost = Cost;
        this.Paid = Paid;
    }

    public void removeItem(Item item){
        this.items.remove(item);
    }

    public void clear(){
        this.items.clear();
        this.Paid = 0;
        this.cost = 0;
    }

    public void showBill(){
        StringBuilder str = new StringBuilder("Bill\n");
        str.append("--------------------\n");
        for(Item i : this.items){
            str.append(i.getName());
            str.append("\t");
            str.append(i.getCost());
            str.append("\n");
        }
        str.append("Total Cost : \t");
        str.append(this.cost);
        str.append("\n");
        str.append("Paid : \t");
        str.append(this.Paid);
        str.append("\n");
        str.append("Change : \t");
        str.append(this.Paid-this.cost);
        System.out.print(str);
    }
}
