public enum Coin {
    Penny(1),Nickel(5), Dime(10),Quarter(25) , oneDollar(100) , twoDollars(200);

    private final int value;

    private Coin(int value){
        this.value=value;
    }
    public int getValue(){
        return this.value;
    }

}
