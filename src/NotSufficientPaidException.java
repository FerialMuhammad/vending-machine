public class NotSufficientPaidException extends RuntimeException {
    private String message;
    NotSufficientPaidException(String msg){
        this.message = msg;
    }
    @Override
    public String getMessage(){
        return this.message;
    }
}
